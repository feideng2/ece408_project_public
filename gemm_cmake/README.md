



<!DOCTYPE html>
<html lang="en" class=" is-copy-enabled is-u2f-enabled">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-cfd028037d00b8a80c5a3b91967e29dcc41309e44d4ccece88663a52963ca765.css" integrity="sha256-z9AoA30AuKgMWjuRln4p3MQTCeRNTM7OiGY6UpY8p2U=" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-dc8832cbc25013ebcdcca24529e402cfcea50e4b2bd136b8284fdf8864873636.css" integrity="sha256-3Igyy8JQE+vNzKJFKeQCz86lDksr0Ta4KE/fiGSHNjY=" media="all" rel="stylesheet" />
    
    
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/site-9e0f35305336555b58884b07a160747fc1f6dbd79e13e18820a598a9abcb2662.css" integrity="sha256-ng81MFM2VVtYiEsHoWB0f8H229eeE+GIIKWYqavLJmI=" media="all" rel="stylesheet" />
    

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=device-width">
    
    <title>ece408project/README.md at master · webgpu/ece408project · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="https://avatars0.githubusercontent.com/u/17249912?v=3&amp;s=400" name="twitter:image:src" /><meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="webgpu/ece408project" name="twitter:title" /><meta content="ece408project - The goal of this project is to accelerate the forward propagation step of the Convolutional Neural Network (CNN) algorithm using GPUs. " name="twitter:description" />
      <meta content="https://avatars0.githubusercontent.com/u/17249912?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="webgpu/ece408project" property="og:title" /><meta content="https://github.com/webgpu/ece408project" property="og:url" /><meta content="ece408project - The goal of this project is to accelerate the forward propagation step of the Convolutional Neural Network (CNN) algorithm using GPUs. " property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    
    <meta name="request-id" content="62D490BD:557B:4925FB3:5855D342" data-pjax-transient>

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

    <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="62D490BD:557B:4925FB3:5855D342" name="octolytics-dimension-request_id" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />



  <meta class="js-ga-set" name="dimension1" content="Logged Out">



        <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

        <meta name="expected-hostname" content="github.com">
      <meta name="js-proxy-site-detection-payload" content="MmUwZjBlZDA0NzhmYjNlYzdlYmI3NDJjZmJjMzNjNTlmY2FmMDE2YjQ4MjBjMDk5Njk4OGIxNzk0M2ZjZDBiZHx7InJlbW90ZV9hZGRyZXNzIjoiOTguMjEyLjE0NC4xODkiLCJyZXF1ZXN0X2lkIjoiNjJENDkwQkQ6NTU3Qjo0OTI1RkIzOjU4NTVEMzQyIiwidGltZXN0YW1wIjoxNDgyMDE5NjUwLCJob3N0IjoiZ2l0aHViLmNvbSJ9">


      <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#000000">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <meta name="html-safe-nonce" content="f0cb0015334d1aa9d75b5545ac87cf97b04b06f0">
    <meta content="bc4c4295c56de55124876b7fd5ddcbeaddca7d1f" name="form-nonce" />

    <meta http-equiv="x-pjax-version" content="8cdbb8cffd6956425aaad8ad14e0496b">
    

      
  <meta name="description" content="ece408project - The goal of this project is to accelerate the forward propagation step of the Convolutional Neural Network (CNN) algorithm using GPUs. ">
  <meta name="go-import" content="github.com/webgpu/ece408project git https://github.com/webgpu/ece408project.git">

  <meta content="17249912" name="octolytics-dimension-user_id" /><meta content="webgpu" name="octolytics-dimension-user_login" /><meta content="73219065" name="octolytics-dimension-repository_id" /><meta content="webgpu/ece408project" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="73219065" name="octolytics-dimension-repository_network_root_id" /><meta content="webgpu/ece408project" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/webgpu/ece408project/commits/master.atom" rel="alternate" title="Recent Commits to ece408project:master" type="application/atom+xml">


      <link rel="canonical" href="https://github.com/webgpu/ece408project/blob/master/README.md" data-pjax-transient>
  </head>


  <body class="logged-out  env-production windows vis-public page-blob">
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"><div class="progress"></div></div>
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>

    
    
    



          <header class="site-header js-details-container" role="banner">
  <div class="container-responsive">
    <a class="header-logo-invertocat" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="32" version="1.1" viewBox="0 0 16 16" width="32"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
    </a>

    <button class="btn-link float-right site-header-toggle js-details-target" type="button" aria-label="Toggle navigation">
      <svg aria-hidden="true" class="octicon octicon-three-bars" height="24" version="1.1" viewBox="0 0 12 16" width="18"><path fill-rule="evenodd" d="M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z"/></svg>
    </button>

    <div class="site-header-menu">
      <nav class="site-header-nav site-header-nav-main">
        <a href="/personal" class="js-selected-navigation-item nav-item nav-item-personal" data-ga-click="Header, click, Nav menu - item:personal" data-selected-links="/personal /personal">
          Personal
</a>        <a href="/open-source" class="js-selected-navigation-item nav-item nav-item-opensource" data-ga-click="Header, click, Nav menu - item:opensource" data-selected-links="/open-source /open-source">
          Open source
</a>        <a href="/business" class="js-selected-navigation-item nav-item nav-item-business" data-ga-click="Header, click, Nav menu - item:business" data-selected-links="/business /business/partners /business/features /business/customers /business">
          Business
</a>        <a href="/explore" class="js-selected-navigation-item nav-item nav-item-explore" data-ga-click="Header, click, Nav menu - item:explore" data-selected-links="/explore /trending /trending/developers /integrations /integrations/feature/code /integrations/feature/collaborate /integrations/feature/ship /showcases /explore">
          Explore
</a>      </nav>

      <div class="site-header-actions">
            <a class="btn btn-primary site-header-actions-btn" href="/join?source=header-repo" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
          <a class="btn site-header-actions-btn mr-1" href="/login?return_to=%2Fwebgpu%2Fece408project%2Fblob%2Fmaster%2FREADME.md" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
      </div>

        <nav class="site-header-nav site-header-nav-secondary mr-md-3">
          <a class="nav-item" href="/pricing">Pricing</a>
          <a class="nav-item" href="/blog">Blog</a>
          <a class="nav-item" href="https://help.github.com">Support</a>
          <a class="nav-item header-search-link" href="https://github.com/search">Search GitHub</a>
              <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="/webgpu/ece408project/search" class="js-site-search-form" data-scoped-search-url="/webgpu/ece408project/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
      <div class="header-search-scope">This repository</div>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        autocapitalize="off">
    </label>
</form></div>

        </nav>
    </div>
  </div>
</header>



    <div id="start-of-content" class="accessibility-aid"></div>

      <div id="js-flash-container">
</div>


    <div role="main">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>
      
<div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
  <div class="container repohead-details-container">

    

<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2Fwebgpu%2Fece408project"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z"/></svg>
    Watch
  </a>
  <a class="social-count" href="/webgpu/ece408project/watchers"
     aria-label="6 users are watching this repository">
    6
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fwebgpu%2Fece408project"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"/></svg>
    Star
  </a>

    <a class="social-count js-social-count" href="/webgpu/ece408project/stargazers"
      aria-label="2 users starred this repository">
      2
    </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fwebgpu%2Fece408project"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path fill-rule="evenodd" d="M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
        Fork
      </a>

    <a href="/webgpu/ece408project/network" class="social-count"
       aria-label="11 users forked this repository">
      11
    </a>
  </li>
</ul>

    <h1 class="public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z"/></svg>
  <span class="author" itemprop="author"><a href="/webgpu" class="url fn" rel="author">webgpu</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/webgpu/ece408project" data-pjax="#js-repo-pjax-container">ece408project</a></strong>

</h1>

  </div>
  <div class="container">
    
<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/webgpu/ece408project" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /webgpu/ece408project" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"/></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/webgpu/ece408project/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /webgpu/ece408project/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"/></svg>
        <span itemprop="name">Issues</span>
        <span class="counter">0</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/webgpu/ece408project/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /webgpu/ece408project/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"/></svg>
      <span itemprop="name">Pull requests</span>
      <span class="counter">0</span>
      <meta itemprop="position" content="3">
</a>  </span>

  <a href="/webgpu/ece408project/projects" class="js-selected-navigation-item reponav-item" data-selected-links="repo_projects new_repo_project repo_project /webgpu/ece408project/projects">
    <svg aria-hidden="true" class="octicon octicon-project" height="16" version="1.1" viewBox="0 0 15 16" width="15"><path fill-rule="evenodd" d="M10 12h3V2h-3v10zm-4-2h3V2H6v8zm-4 4h3V2H2v12zm-1 1h13V1H1v14zM14 0H1a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h13a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1z"/></svg>
    Projects
    <span class="counter">0</span>
</a>


  <a href="/webgpu/ece408project/pulse" class="js-selected-navigation-item reponav-item" data-selected-links="pulse /webgpu/ece408project/pulse">
    <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z"/></svg>
    Pulse
</a>
  <a href="/webgpu/ece408project/graphs" class="js-selected-navigation-item reponav-item" data-selected-links="repo_graphs repo_contributors /webgpu/ece408project/graphs">
    <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z"/></svg>
    Graphs
</a>

</nav>

  </div>
</div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">

    

<a href="/webgpu/ece408project/blob/7df83b8a66745187db1012e093d14346767044e1/README.md" class="d-none js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:5dc0b38d74dfbeead700f90c1fa2620f -->

<div class="file-navigation js-zeroclipboard-container">
  
<div class="select-menu branch-select-menu js-menu-container js-select-menu float-left">
  <button class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    
    type="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/webgpu/ece408project/blob/master/README.md"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"/></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text">
                master
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

  <div class="BtnGroup float-right">
    <a href="/webgpu/ece408project/find/master"
          class="js-pjax-capture-input btn btn-sm BtnGroup-item"
          data-pjax
          data-hotkey="t">
      Find file
    </a>
    <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm BtnGroup-item tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
  </div>
  <div class="breadcrumb js-zeroclipboard-target">
    <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/webgpu/ece408project"><span>ece408project</span></a></span></span><span class="separator">/</span><strong class="final-path">README.md</strong>
  </div>
</div>


  <div class="commit-tease">
      <span class="float-right">
        <a class="commit-tease-sha" href="/webgpu/ece408project/commit/7df83b8a66745187db1012e093d14346767044e1" data-pjax>
          7df83b8
        </a>
        <relative-time datetime="2016-12-06T14:20:41Z">Dec 6, 2016</relative-time>
      </span>
      <div>
        <img alt="@abduld" class="avatar" height="20" src="https://avatars3.githubusercontent.com/u/1404191?v=3&amp;s=40" width="20" />
        <a href="/abduld" class="user-mention" rel="contributor">abduld</a>
          <a href="/webgpu/ece408project/commit/7df83b8a66745187db1012e093d14346767044e1" class="message" data-pjax="true" title="add link">add link</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>3</strong>
         contributors
      </button>
          <a class="avatar-link tooltipped tooltipped-s" aria-label="abduld" href="/webgpu/ece408project/commits/master/README.md?author=abduld"><img alt="@abduld" class="avatar" height="20" src="https://avatars3.githubusercontent.com/u/1404191?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="cli99" href="/webgpu/ece408project/commits/master/README.md?author=cli99"><img alt="@cli99" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/17418037?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="cwpearson" href="/webgpu/ece408project/commits/master/README.md?author=cwpearson"><img alt="@cwpearson" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/6765756?v=3&amp;s=40" width="20" /> </a>


    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@abduld" height="24" src="https://avatars1.githubusercontent.com/u/1404191?v=3&amp;s=48" width="24" />
            <a href="/abduld">abduld</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@cli99" height="24" src="https://avatars2.githubusercontent.com/u/17418037?v=3&amp;s=48" width="24" />
            <a href="/cli99">cli99</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@cwpearson" height="24" src="https://avatars0.githubusercontent.com/u/6765756?v=3&amp;s=48" width="24" />
            <a href="/cwpearson">cwpearson</a>
          </li>
      </ul>
    </div>
  </div>


<div class="file">
  <div class="file-header">
  <div class="file-actions">

    <div class="BtnGroup">
      <a href="/webgpu/ece408project/raw/master/README.md" class="btn btn-sm BtnGroup-item" id="raw-url">Raw</a>
        <a href="/webgpu/ece408project/blame/master/README.md" class="btn btn-sm js-update-url-with-hash BtnGroup-item">Blame</a>
      <a href="/webgpu/ece408project/commits/master/README.md" class="btn btn-sm BtnGroup-item" rel="nofollow">History</a>
    </div>

        <a class="btn-octicon tooltipped tooltipped-nw"
           href="https://windows.github.com"
           aria-label="Open this file in GitHub Desktop"
           data-ga-click="Repository, open with desktop, type:windows">
            <svg aria-hidden="true" class="octicon octicon-device-desktop" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M15 2H1c-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h5.34c-.25.61-.86 1.39-2.34 2h8c-1.48-.61-2.09-1.39-2.34-2H15c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm0 9H1V3h14v8z"/></svg>
        </a>

        <button type="button" class="btn-octicon disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path fill-rule="evenodd" d="M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z"/></svg>
        </button>
        <button type="button" class="btn-octicon btn-octicon-danger disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"/></svg>
        </button>
  </div>

  <div class="file-info">
      356 lines (240 sloc)
      <span class="file-info-divider"></span>
    19.1 KB
  </div>
</div>

  
  <div id="readme" class="readme blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="text"><h1><a id="user-content-ece-408-project" class="anchor" href="#ece-408-project" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>ECE 408 Project</h1>

<p>The goal of this project is to accelerate the forward propagation step of the Convolutional Neural Network (CNN) algorithm using GPUs. The sequential implementation provided follows the basic algorithm 16.4 and 16.5 decribed in <a href="https://wiki.illinois.edu/wiki/display/ece408f16/Book+Chapters?preview=/602518692/603851747/3rd-Edition-Chapter16-case-study-DNN-FINAL.pdf">book chapter 16</a>. The dataset and model are from the <a href="http://yann.lecun.com/exdb/mnist/">MNIST database</a>.</p>

<h2><a id="user-content-cnn-and-mnist" class="anchor" href="#cnn-and-mnist" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>CNN and MNIST</h2>

<p>Read the book chapter and familiarize youself with the CNN algorithm.</p>

<p>Provided is a model that has been trained using 60,000 examples (training set images) and the provided test data is 10,000 batched queries (test set images). The expected accuracy of the CNN is <code>~87%</code> on the provided test dataset.</p>

<p>The data and model are in <a href="https://support.hdfgroup.org/HDF5/">HDF5</a> format and we have provided the code to read the input model and the training dataset.</p>

<h2><a id="user-content-cuda-implementation" class="anchor" href="#cuda-implementation" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>CUDA Implementation</h2>

<p>Book chapters 16.3 and 16.4 provide a basic CUDA implementation of forward propagation of convolutional layer and possible optimization. Your CUDA implementation would be evaluated based on performance and accuracy. Apply any optimization you think would bring benefit and feel free to modify any part of the code. You should not use <code>cuBLAS</code> or <code>cuDNN</code> for the implementation, but you are expected to compare your implementation with those libraries --- profiling the code as well as comparing the algorithms used (if algorithm information is publically available).</p>

<h2><a id="user-content-remote-development-environment" class="anchor" href="#remote-development-environment" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Remote Development Environment</h2>

<p>The easiest way to develop the project is to use rai through the following prebuilt binaries. The stable version only supports Linux and OSX. For students with Windows, you can try the beta version or use the Linux on <a href="http://it.engineering.illinois.edu/ews">EWS</a> for RAI.</p>

<p><strong>NOTE:</strong> Even if you use your local development environment, your final code must run within the RAI system. Also, your final report performance measurements must be done within RAI.</p>

<h3><a id="user-content-download-binaries" class="anchor" href="#download-binaries" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Download Binaries</h3>

<p>The code is continuously built and published. The client can be downloaded from the following URLs (depending on your OS and Architecture):</p>

<table><thead>
<tr>
<th>Operating System</th>
<th>Architecture</th>
<th>Stable Version Link</th>
<th>Development Version Link</th>
</tr>
</thead><tbody>
<tr>
<td>Linux</td>
<td>i386</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-386.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-386.tar.gz">URL</a></td>
</tr>
<tr>
<td>Linux</td>
<td>amd64</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-amd64.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-amd64.tar.gz">URL</a></td>
</tr>
<tr>
<td>Linux</td>
<td>armv5</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-armv5.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-armv5.tar.gz">URL</a></td>
</tr>
<tr>
<td>Linux</td>
<td>armv6</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-armv6.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-armv6.tar.gz">URL</a></td>
</tr>
<tr>
<td>Linux</td>
<td>armv7</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-armv7.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-armv7.tar.gz">URL</a></td>
</tr>
<tr>
<td>Linux</td>
<td>arm64</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-linux-arm64.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-linux-arm64.tar.gz">URL</a></td>
</tr>
<tr>
<td>OSX/Darwin</td>
<td>i386</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-darwin-386.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-darwin-386.tar.gz">URL</a></td>
</tr>
<tr>
<td>OSX/Darwin</td>
<td>amd64</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-darwin-amd64.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-darwin-amd64.tar.gz">URL</a></td>
</tr>
<tr>
<td>Windows</td>
<td>i386</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-windows-386.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-windows-386.tar.gz">URL</a></td>
</tr>
<tr>
<td>Windows</td>
<td>amd64</td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/rai-windows-amd64.tar.gz">URL</a></td>
<td><a href="http://rai-server.s3.amazonaws.com/dist/dev/rai-windows-amd64.tar.gz">URL</a></td>
</tr>
</tbody></table>

<h3><a id="user-content-client" class="anchor" href="#client" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Client</h3>

<h4><a id="user-content-set-up-your-secret-key" class="anchor" href="#set-up-your-secret-key" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Set up your Secret Key</h4>

<p>Each team will be contacted by a TA and given a secret key to use this service. Do not share your key with other teams. The secret key is used to authenticate you with the server.</p>

<p>The <code>RAI_SECRET_KEY</code>, <code>RAI_TEAM_NAME</code>, and <code>RAI_ACCESS_KEY</code> should be specified in your <code>~/.rai.profile</code> (linux/OSX) or <code>%HOME%/.rai.profile</code> (Windows -- for me this is <code>C:\Users\abduld\.rai.profile</code>) in the following way.</p>

<div class="highlight highlight-source-shell"><pre>RAI_TEAM_NAME=<span class="pl-s"><span class="pl-pds">'</span>Your Team Name Here<span class="pl-pds">'</span></span>
RAI_USER_NAME=<span class="pl-s"><span class="pl-pds">'</span>user<span class="pl-pds">'</span></span>
RAI_ACCESS_KEY=<span class="pl-s"><span class="pl-pds">'</span>XXXXXXXX<span class="pl-pds">'</span></span>
RAI_SECRET_KEY=<span class="pl-s"><span class="pl-pds">'</span>XXXXX<span class="pl-pds">'</span></span></pre></div>

<p>The above will need to match the email you recieved from <code>postmaster@webgpu.com</code> on Nov 23. If you did not recieve the email, then contact the TA. Also, contact the TA with your team name as soon as possible.  Do not share your keys with other users or teams. The access and secret key is used to authenticate you with the server. Both the team name and the username are used to identify you to the system.</p>

<h4><a id="user-content-run-the-client" class="anchor" href="#run-the-client" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Run the Client</h4>

<p>To run the client, use</p>

<div class="highlight highlight-source-shell"><pre>rai -d <span class="pl-k">&lt;</span>project folder<span class="pl-k">&gt;</span></pre></div>

<p>From a user's point a view when the client runs, the local directory specified by <code>-d</code> gets uploaded to the server and extracted into the <code>/src</code> directory on the server. The server then executes the build commands from the <code>rai-build.yml</code> specification within the <code>/build</code> directory. Once the commands have been run, or there is an error, a zipped version of that <code>/build</code> directory is available from the server for download.</p>

<p>The server limits the task time to be an hour with a maximum of 8GB of memory being used within a session. The output <code>/build</code> directory is only available to be downloaded from the server for a short amount of time. Networking is also disabled on the execution server.</p>

<h4><a id="user-content-internal-details-ignore-if-not-interested" class="anchor" href="#internal-details-ignore-if-not-interested" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Internal Details (Ignore if not Interested)</h4>

<p>The client sends job submission requests to the rai server. The internal steps the client takes are as follows:</p>

<ol>
<li> The client creates an archive of your directory and posts it to Amazon S3</li>
<li> The client creates a unique identifier (here called <code>ID</code>). These IDs are generated using <a href="https://godoc.org/labix.org/v2/mgo/bson#NewObjectId"><code>NewObjectId</code></a>.</li>
<li> The client creates a job request and publishes to the <code>tasks</code> topic on the queue. The job request has the ID field with the value <code>ID</code> and is mashaled using using the <a href="https://godoc.org/labix.org/v2/mgo/bson"><code>bson</code></a> library. The reason for using <code>bson</code> is that we will want to store the results in mongodb in the future.</li>
<li> The client subscribes to the topic <code>log-ID</code> and prints the results on that topic.</li>
<li> The client stops listening when the message on the topic has a tag <code>TagEnd</code>.</li>
</ol>

<h3><a id="user-content-project-build-sepecification" class="anchor" href="#project-build-sepecification" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Project Build Sepecification</h3>

<p>The <code>rai-build.yml</code> must exist in your project directory. If not available, then the system will use the default build script. In some cases you may not be able to execute certain commands, in this senario the current workaround is to create a bash file and insert the commands you need to run. You can then execute the bash script within <code>rai-build.yml</code>.</p>

<p>The <code>rai-build.yml</code> is written as a <a href="http://yaml.org/">Yaml</a> (<a href="http://www.yaml.org/spec/1.2/spec.html">Spec</a>) file and has the following structure.</p>

<div class="highlight highlight-source-yaml"><pre><span class="pl-ent">rai:</span>
  <span class="pl-ent">version:</span> <span class="pl-c1">0.1</span> <span class="pl-c"># this is required</span>
  <span class="pl-ent">image:</span> <span class="pl-s">webgpu/rai:root </span><span class="pl-c"># this is ignored at this moment with the webgpu/rai:root</span>
                         <span class="pl-c"># image being used by default. webgpu/rai:root is a docker</span>
                         <span class="pl-c"># image which can be viewed at https://hub.docker.com/r/webgpu/rai/</span>
<span class="pl-ent">resources:</span>
  <span class="pl-ent">gpus:</span> <span class="pl-c1">1</span> <span class="pl-c"># currently this field is ignored, but in the future you'd be able to specify your</span>
          <span class="pl-c"># system requirements</span>
<span class="pl-ent">commands:</span>
  <span class="pl-ent">build:</span>
    - <span class="pl-s">echo "Building project"</span>
<span class="pl-s">    </span><span class="pl-c"># Since the system already contains the dependencies (like HDF5 and ZLib) we do not</span>
<span class="pl-s">    </span><span class="pl-c"># need the hunter package manager. This speeds up the compilation as well</span>
    - <span class="pl-s">cmake -DCONFIG_USE_HUNTER=OFF /src</span>
<span class="pl-s">    </span><span class="pl-c"># Run the make file to compile the project.</span>
    - <span class="pl-s">make</span>
<span class="pl-s">    </span><span class="pl-c"># here we break the long command into multiple lines. The Yaml</span>
<span class="pl-s">    </span><span class="pl-c"># format supports this using a block-strip command. See</span>
    <span class="pl-c"># http://stackoverflow.com/a/21699210/3543720 for info</span>
    - <span class="pl-s">&gt;-</span>
<span class="pl-s">      nvprof --analysis-metrics --print-api-trace --cpu-profiling on</span>
<span class="pl-s">      --demangling on --export-profile profile.nvvp</span>
<span class="pl-s">      --force-overwrite --log-file run.log --print-gpu-trace</span>
<span class="pl-s">      -- ./ece408 /src/data/test10.hdf5 /src/data/model.hdf5 10</span></pre></div>

<p>Syntax errors will be reported and the job will not be executed. You can check if your file is in a valid yaml format by using tools such as <a href="http://codebeautify.org/yaml-validator">Yaml Validator</a>.</p>

<h2><a id="user-content-profiling" class="anchor" href="#profiling" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Profiling</h2>

<p>Profiling can be performed using <code>nvprof</code>. Place the following build commands in your <code>rai-build.yml</code> file</p>

<div class="highlight highlight-source-yaml"><pre>    - <span class="pl-s">&gt;-</span>
<span class="pl-s">      nvprof --cpu-profiling on --export-profile timeline.nvprof --</span>
<span class="pl-s">      ./ece408 /src/data/test10.hdf5 /src/data/model.hdf5 10</span>
<span class="pl-s"></span>    - <span class="pl-s">&gt;-</span>
<span class="pl-s">      nvprof --cpu-profiling on --export-profile analysis.nvprof --analysis-metrics --</span>
<span class="pl-s">      ./ece408 /src/data/test10.hdf5 /src/data/model.hdf5 10</span></pre></div>

<p>You could change the input and test datasets. This will output two files <code>timeline.nvprof</code> and <code>analysis.nvprof</code> which can be viewed using the <code>nvvp</code> tool (by performing a <code>file&gt;import</code>).</p>

<p><em>NOTE:</em> <code>nvvp</code> will only show performance metrics for GPU invocations, so it may not show any analysis when you only have serial code.</p>

<h3><a id="user-content-project-submission" class="anchor" href="#project-submission" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Project Submission</h3>

<p>You will use the same client (with certain options) for the final submission. The submission system notify the teaching assistants and record your ranking. You will need the above credentials to make your final submission.</p>

<p>To submit your project, run</p>

<div class="highlight highlight-source-shell"><pre>rai submit -d <span class="pl-k">&lt;</span>project folder<span class="pl-k">&gt;</span></pre></div>

<p>To perform the final project submission, you must have the <code>USAGE</code>, <code>README</code>, and <code>report.pdf</code> files in your project folder (as stated in the <a href="#what-to-deliver">"What to Deliver"</a> section). The submission system ignores your <code>rai-build.yml</code> file and instead runs the following build file:</p>

<div class="highlight highlight-source-yaml"><pre><span class="pl-ent">rai:</span>
  <span class="pl-ent">version:</span> <span class="pl-c1">0.1</span>
<span class="pl-ent">resources:</span>
  <span class="pl-ent">gpus:</span> <span class="pl-c1">1</span>
<span class="pl-ent">commands:</span>
  <span class="pl-ent">build:</span>
    - <span class="pl-s">echo "Submitting project"</span>
    - <span class="pl-s">cp -r /src /build/submission_code</span>
    - <span class="pl-s">cmake -DCONFIG_USE_HUNTER=OFF /src</span>
    - <span class="pl-s">make</span>
    - <span class="pl-s">/usr/bin/time ./ece408 /src/data/testfull.hdf5 /src/data/model.hdf5 10000</span></pre></div>

<p><strong>NOTE::</strong> Only your last submission is recorded, so please make sure that your last submission is the one you'd want to be graded.</p>

<h3><a id="user-content-competition-rankings" class="anchor" href="#competition-rankings" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Competition Rankings</h3>

<p>You can see the current rankings for the project competition by invoking</p>

<div class="highlight highlight-source-shell"><pre>rai rankings</pre></div>

<p>You can see only the top 10 teams by invoking</p>

<div class="highlight highlight-source-shell"><pre>rai rankings -l 10</pre></div>

<h3><a id="user-content-reporting-issues" class="anchor" href="#reporting-issues" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Reporting Issues</h3>

<p>If emailing the TA with a problem, then please include the output of</p>

<pre><code>rai version
</code></pre>

<p>as well as the output of</p>

<pre><code>rai buildtime
</code></pre>

<p>you can also invoke the rai command with verbose and debug outputs using</p>

<pre><code>rai --verbose --debug
</code></pre>

<h2><a id="user-content-local-development-environment" class="anchor" href="#local-development-environment" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Local Development Environment</h2>

<p><strong>NOTE:</strong> Even if you use your local development environment, your final code must run within the RAI system. Also, your final report performance measurements must be done within RAI.</p>

<p>The project requires a CUDA-supported operating system, C compiler, and the CUDA 8 Toolkit. The CUDA 8 Toolkit can be downloaded from the <a href="https://developer.nvidia.com/cuda-downloads">CUDA Download</a> page. Instructions on how to install the CUDA Toolkit are available in the <a href="http://docs.nvidia.com/cuda/cuda-quick-start-guide/index.html">Quick Start page</a>. Installation guides and the list of supported C compilers for <a href="http://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html">Windows</a>, <a href="http://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html">Linux</a>, and <a href="http://docs.nvidia.com/cuda/cuda-installation-guide-mac-os-x/index.html">OSX</a> are also found in the <a href="http://docs.nvidia.com/cuda/index.html">CUDA Toolkit Documentation Page</a>.</p>

<p>Aside from a C compiler and the CUDA 8 Toolkit, <a href="https://cmake.org/">CMake</a> 3.1 or later is required to generate build scripts for your target IDE and compiler. On windows, we require Visual Studio 2015 (Service Pack 3) which you can download from the webstore. For other systems, a CUDA compatible compiler is required (e.g. for OSX the <a href="http://docs.nvidia.com/cuda/cuda-installation-guide-mac-os-x/index.html#system-requirements">clang compiler</a> is the only one supported).</p>

<h3><a id="user-content-how-to-build" class="anchor" href="#how-to-build" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>How to Build</h3>

<p>There are two options to build this project, the first is using the <a href="https://github.com/ruslo/hunter">Hunter</a> package manager and the other is using <a href="https://www.docker.com/">Docker</a>. We sugguest using CMake along with Hunter, but it is known not to work on all operating systems. In this case, we suggest that you either using Docker or install the libraries needed (mainly <code>HDF5</code>).</p>

<h4><a id="user-content-using-hunter-package-manager" class="anchor" href="#using-hunter-package-manager" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using Hunter Package Manager</h4>

<p>By default, the compilation uses the <a href="https://github.com/ruslo/hunter">Hunter</a> --- a C package manager. This method requires that you have the CUDA toolkit installed on your machine.</p>

<p>Assuming that you have checked out the project into <code>$SRCDIR</code> do</p>

<div class="highlight highlight-source-shell"><pre><span class="pl-c1">cd</span> <span class="pl-smi">$SRCDIR</span>
mkdir build
<span class="pl-c1">cd</span> build
cmake <span class="pl-smi">$SRCDIR</span></pre></div>

<p>This will download the required software needed for the project (see the <a href="https://docs.hunter.sh/en/latest/">hunter docs</a> for more information). You may see some warning while the system is compiling <em>HDF5</em>, which you can ignore. Once CMake has been run, a <code>Makefile</code> is generated so you can then perform <code>make</code> to buidl the project.</p>

<div class="highlight highlight-source-shell"><pre>make</pre></div>

<p>If you do not plan on using <code>make</code>, examine the <code>cmake -G</code> option which allows you to generate XCode, Visual Studio, ... project configurations. You may also need to change the build type to enable/disable debugging and/or optimizations.</p>

<p>If you need to use another library, you need have to modify the <a href="https://github.com/webgpu/ece408project/blob/master/CMakeLists.txt"><code>CMakeLists.txt</code></a> and add the libraries to the <code>target_link_libraries</code> (and possibly the <code>include_directories</code>) section. Documentation on the CMake commands is found in the <a href="https://cmake.org/cmake/help/latest/">documentation page</a>.</p>

<h4><a id="user-content-using-docker-container" class="anchor" href="#using-docker-container" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using Docker Container</h4>

<p><a href="https://hub.docker.com/r/webgpu/ece408project/"><img src="https://camo.githubusercontent.com/c860f054353d1e72b7a74d4cc3807ba2c982c980/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f6175746f6d617465642f6a726f7474656e626572672f66666d7065672e737667" alt="Docker Automated build" data-canonical-src="https://img.shields.io/docker/automated/jrottenberg/ffmpeg.svg" style="max-width:100%;"></a></p>

<p>Also included is a <a href="http://docker.io/">Docker</a> build file. This file is a specification for a Docker container image. It can be used to build and launch a container (think of a virtual machine) which contains this project along with all the software required to run it. Using a GPU within Docker is only supported on Linux(you can compile and run the serial code on any operating system), and we recommend using <a href="https://github.com/NVIDIA/nvidia-docker">NVIDIA-Docker</a> to run the Docker image. To build the Docker container, do</p>

<div class="highlight highlight-source-shell"><pre><span class="pl-c1">cd</span> <span class="pl-smi">$SRCDIR</span>
docker build <span class="pl-c1">.</span> -t ece408project</pre></div>

<p>Once built, the <code>ece408project</code> image would be listed by the <code>docker images</code> command. This will compile your project. You can launch the docker image using</p>

<div class="highlight highlight-source-shell"><pre>docker run -it ece408project</pre></div>

<h3><a id="user-content-running-the-serial-code" class="anchor" href="#running-the-serial-code" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Running the Serial Code</h3>

<div class="highlight highlight-source-shell"><pre>./ece408 ../data/test10.hdf5 ../data/model.hdf5 batch_size</pre></div>

<p>the <code>batch_size</code> must match the size of the dataset. If <code>batch_size</code> is unspecified, the default value is dependent on the input (10 for "../data/test10.hdf5", ..., 10000 for "../data/testfull.hdf5"), which is also the size of <code>data.hdf5</code>.</p>

<h2><a id="user-content-how-to-test" class="anchor" href="#how-to-test" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>How to Test</h2>

<p>Test your implementation with small batch size frist to verify the correctness. You can parse the <code>data/test100.hdf5</code> into smaller chunks using your preferred language(e.g. python). 2, 10 and 100 queries are provides in <code>data/test2.hdf5</code>, <code>data/test10.hdf5</code> and <code>data/test100.hdf5</code> in the data folder. Maker sure the data file you feed in has the same batch size as the <code>batch_size</code> you specify in the command line.</p>

<div class="highlight highlight-source-shell"><pre>./ece408 ../data/test10.hdf5 ../data/model.hdf5 10</pre></div>

<h2><a id="user-content-what-to-deliver" class="anchor" href="#what-to-deliver" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>What to Deliver</h2>

<p>A <code>.tar.gz</code> file which contains the report, code directory, the build scripts, and, possibly, the input dataset needs to be delivered to the Teaching Assistants.</p>

<ul>
<li>  Code:  A <code>USAGE</code> file needs to be placed included in the archive file which includes instructions on how to compile and run your code. If the report performs any profiling, the <code>USAGE</code> file must also specify how to run the performance measurements.</li>
<li>  Report: A PDF version report must be included within the <code>.tar.gz</code> file. The report should describe and evaluate the optimizations you tried. The report does not have a page limit, but as usual, you should strive to be thorough, concise, and quantitative in your performance analysis.
The report must be named <code>report.pdf</code></li>
</ul>

<p>Make sure you have a working CUDA implementation before applying any optimizations.</p>

<h2><a id="user-content-optimization-opportunities" class="anchor" href="#optimization-opportunities" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Optimization Opportunities</h2>

<p>The serial version of the code is amicable to many optimization opportunities, the following is an incomplete set of them:</p>

<ul>
<li>  Optimize the CUDA memory copies to decrease the overhead of memory transfers</li>
<li>  Overlapping the memory transfer and the compute and/or independent computations using CUDA streams</li>
<li>  Performing layout transformations to get coallessed accesses or to make better use of the cache</li>
<li>  Using low precision to perform the computation (for example using <code>float16</code> or binary values)</li>
<li>  Based on the size of the convolution, utilitize better algorithms to perform the computation (for example using the [Winograd Kernel][<a href="https://www.nervanasys.com/winograd-2/">https://www.nervanasys.com/winograd-2/</a>])</li>
</ul>

<h2><a id="user-content-utility-functions" class="anchor" href="#utility-functions" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Utility Functions</h2>

<p>We provide a some helper utility functions in the <a href="https://github.com/webgpu/ece408project/blob/master/src/utils.hpp"><code>utils.hpp</code></a> file.</p>

<h3><a id="user-content-how-to-time" class="anchor" href="#how-to-time" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>How to Time</h3>

<p>In <a href="https://github.com/webgpu/ece408project/blob/master/src/utils.hpp"><code>utils.hpp</code></a> a function called <code>now()</code> which allows you to get the current time at a high resolution. To measure the overhead of a function <code>f(args...)</code>, the pattern to use is:</p>

<div class="highlight highlight-source-c++"><pre><span class="pl-k">const</span> <span class="pl-k">auto</span> tic = now();
<span class="pl-en">f</span>(args...);
<span class="pl-k">const</span> <span class="pl-k">auto</span> toc = now();
<span class="pl-k">const</span> <span class="pl-k">auto</span> elapsed = std::chrono::duration&lt;<span class="pl-k">double</span>, std::milli&gt;(toc - tic).count();;
std::cout &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span>Calling f(args...) took <span class="pl-pds">"</span></span> &lt;&lt; elapsed &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span>milliseconds<span class="pl-cce">\n</span><span class="pl-pds">"</span></span>;</pre></div>

<h3><a id="user-content-range-for-loops" class="anchor" href="#range-for-loops" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Range For Loops</h3>

<p>Throughout the serial code, we use the <a href="https://github.com/harrism/cpp11-range"><code>range.hpp</code></a> to make the code easier to understand. Essentially,</p>

<div class="highlight highlight-source-c++"><pre><span class="pl-k">for</span> (<span class="pl-k">const</span> <span class="pl-k">auto</span> ii : range(<span class="pl-c1">0</span>, N)) {
    <span class="pl-c1">do_stuff</span>(ii);
}</pre></div>

<p>Is equivalent to</p>

<div class="highlight highlight-source-c++"><pre><span class="pl-k">for</span> (<span class="pl-k">const</span> <span class="pl-k">auto</span> ii = <span class="pl-c1">0</span>; ii &lt; N; ii++) {
    <span class="pl-c1">do_stuff</span>(ii);
}</pre></div>

<h3><a id="user-content-checking-errors" class="anchor" href="#checking-errors" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Checking Errors</h3>

<p>To check for CUDA errors, specialize the <code>check_success</code> function in <code>utils.hpp</code> to also handle <code>cudaError_t</code>. For example:</p>

<div class="highlight highlight-source-c++"><pre><span class="pl-k">template </span>&lt;&gt;
<span class="pl-k">bool</span> check_success&lt;cudaError_t&gt;(<span class="pl-k">const</span> cudaError_t &amp;err) {
  <span class="pl-k">const</span> <span class="pl-k">auto</span> res = err == cudaSuccess;
  <span class="pl-k">if</span> (res == <span class="pl-c1">true</span>) {
    <span class="pl-k">return</span> res;
  }
  std::cout &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span>Failed in CUDA. Error = <span class="pl-pds">"</span></span> &lt;&lt; <span class="pl-c1">cudaGetErrorString</span>(err) &lt;&lt; std::endl;
  <span class="pl-c1">assert</span>(res);
  <span class="pl-k">return</span> res;
}</pre></div>

<p><code>check_success</code> can then be used when calling CUDA functions:</p>

<div class="highlight highlight-source-c++"><pre><span class="pl-en">check_success</span>(cudaFree(deviceData));</pre></div>

<h2><a id="user-content-reporting-issues-1" class="anchor" href="#reporting-issues-1" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Reporting Issues</h2>

<p>Please use the <a href="https://github.com/webgpu/ece408project/issues">Github issue manager</a> to report any issues or suggestions about the project.</p>
</article>
  </div>

</div>

<button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="d-none">Jump to Line</button>
<div id="jump-to-line" style="display:none">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

  </div>
  <div class="modal-backdrop js-touch-events"></div>
</div>


    </div>
  </div>

    </div>

        <div class="container site-footer-container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links float-right">
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact GitHub</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"/></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2016 <span title="0.03772s from github-fe-651e69d.cp1-iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



    

    <div id="ajax-error-message" class="ajax-error-message flash flash-error">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
      <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
        <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
      </button>
      You can't perform that action at this time.
    </div>


      
      <script crossorigin="anonymous" integrity="sha256-BciiVpjF80z81NbTK5C+pYJJy0AR3716p9iUKqiUi6A=" src="https://assets-cdn.github.com/assets/frameworks-05c8a25698c5f34cfcd4d6d32b90bea58249cb4011dfbd7aa7d8942aa8948ba0.js"></script>
      <script async="async" crossorigin="anonymous" integrity="sha256-ui8c6b8Kx6xyTL16Zl4BliNXtzMyLWgscQqAIibkRaE=" src="https://assets-cdn.github.com/assets/github-ba2f1ce9bf0ac7ac724cbd7a665e01962357b733322d682c710a802226e445a1.js"></script>
      
      
      
      
    <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner d-none">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"/></svg>
      <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
      <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
    </div>
    <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"/></svg>
    </button>
  </div>
</div>

  </body>
</html>

